<?php

namespace App\Http\Controllers;
use App\Model\admin\mover;

use Illuminate\Http\Request;


class MoverController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
     $movers = mover::all();
     return view('admin.home', compact('movers'));
       // return mover::pluck('id');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //return 'hi';
        $mover = new mover;
        $this->validate($request,[
            'title'=>'required|unique:movers',
            'body'=>'required|unique:movers',
        ]);
        $mover->title = $request->title;
        $mover->body = $request->body;
        $mover->save();
        return redirect('admin/mover');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $item = mover::find($id);
        return view('admin.show', compact('item'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $item = mover::find($id);
        return view('admin.edit', compact('item'));

     //  return 'hi';
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $mover = mover::find($id);
        $this->validate($request,[
            'title'=>'required',
            'body'=>'required',
        ]);
        $mover->title = $request->title;
        $mover->body = $request->body;
        $mover->save();
        session()->flash('messege', 'Updated Successfully');
        return redirect('admin/mover');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $mover = mover::find($id);
        $mover->delete();
        session()->flash('messege', 'Deleted Successfully');
        return redirect('admin/mover');
    }
}
