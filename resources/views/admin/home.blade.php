@extends('admin.layouts.app')

@section('sidebarMenu')
    <!-- Start .sidebar-scrollarea -->
    <div class="sidebar-scrollarea">
        <div class="sidenav">
            <div class="sidebar-widget mb0">
                <h6 class="title mb0">Navigation</h6>
            </div>
            <!-- End .sidenav-widget -->
            <div class="mainnav">
                <ul>
                    <li><a href="index.html"><i class="s16 fa fa-dashboard"></i><span class="txt">Dashboard</span> </a>
                    </li>
                    <li>
                        <a href="#"><i class="s16 icomoon-icon-truck"></i><span class="txt">Movers Company</span> </a>
                        <ul class="sub">
                            <li><a href="mover/create"><i class="s16 entypo-icon-add"></i><span class="txt">Add Mover</span></a>
                            </li>
                            <li><a href="charts-rickshaw.html"><i class="s16 fa fa-gears"></i><span class="txt">Manage Movers</span></a>
                            </li>
                            <li><a href="charts-morris.html"><i class="s16 fa fa-gears"></i><span class="txt">Manage Inactive Movers</span></a>
                            </li>
                            <li><a href="charts-chartjs.html"><i class="s16 fa fa-gears"></i><span class="txt">Manage Company Group</span></a>
                            </li>

                        </ul>
                    </li>
                    <li>
                        <a href="#"><i class="s16 icomoon-icon-bullhorn"></i><span class="txt">Movers Campaigns</span> </a>
                        <ul class="sub">
                            <li><a href="charts-flot.html"><i class="s16 entypo-icon-add"></i><span class="txt">Add Mover</span></a>
                            </li>
                            <li><a href="charts-rickshaw.html"><i class="s16 fa fa-gears"></i><span class="txt">Manage Movers</span></a>
                            </li>

                        </ul>
                    </li>
                    <li>
                        <a href="#"><i class="s16 brocco-icon-database"></i><span class="txt">Payments And Balance</span> </a>
                        <ul class="sub">
                            <li><a href="charts-flot.html"><i class="s16 entypo-icon-add"></i><span class="txt">Add Mover</span></a>
                            </li>
                            <li><a href="charts-rickshaw.html"><i class="s16 fa fa-gears"></i><span class="txt">Manage Movers</span></a>
                            </li>

                        </ul>
                    </li>
                    <li>
                        <a href="#"><i class="s16  icomoon-icon-database"></i><span class="txt">Income Report</span> </a>
                        <ul class="sub">
                            <li><a href="charts-flot.html"><i class="s16 entypo-icon-add"></i><span class="txt">Add Mover</span></a>
                            </li>
                            <li><a href="charts-rickshaw.html"><i class="s16 fa fa-gears"></i><span class="txt">Manage Movers</span></a>
                            </li>

                        </ul>
                    </li>
                    <li>
                        <a href="#"><i class="s16 icomoon-icon-filter"></i><span class="txt">Form Field</span> </a>
                        <ul class="sub">
                            <li><a href="charts-flot.html"><i class="s16 entypo-icon-add"></i><span class="txt">Add Mover</span></a>
                            </li>
                            <li><a href="charts-rickshaw.html"><i class="s16 fa fa-gears"></i><span class="txt">Manage Movers</span></a>
                            </li>

                        </ul>
                    </li>
                    <li>
                        <a href="#"><i class="s16 iconic-icon-info"></i><span class="txt">Recent Leads</span> </a>
                        <ul class="sub">
                            <li><a href="charts-flot.html"><i class="s16 entypo-icon-add"></i><span class="txt">Add Mover</span></a>
                            </li>
                            <li><a href="charts-rickshaw.html"><i class="s16 fa fa-gears"></i><span class="txt">Manage Movers</span></a>
                            </li>

                        </ul>
                    </li>
                    <li>
                        <a href="#"><i class="s16 fa fa-magnet"></i><span class="txt">Re-Broadcasting</span> </a>
                        <ul class="sub">
                            <li><a href="charts-flot.html"><i class="s16 entypo-icon-add"></i><span class="txt">Add Mover</span></a>
                            </li>
                            <li><a href="charts-rickshaw.html"><i class="s16 fa fa-gears"></i><span class="txt">Manage Movers</span></a>
                            </li>

                        </ul>
                    </li>
                    <li>
                        <a href="#"><i class="s16 icomoon-icon-filter"></i><span class="txt">Statewise List</span> </a>
                        <ul class="sub">
                            <li><a href="charts-flot.html"><i class="s16 entypo-icon-add"></i><span class="txt">Add Mover</span></a>
                            </li>
                            <li><a href="charts-rickshaw.html"><i class="s16 fa fa-gears"></i><span class="txt">Manage Movers</span></a>
                            </li>

                        </ul>
                    </li>
                    <li>
                        <a href="#"><i class="s16 icomoon-icon-filter"></i><span class="txt">Websitewise List</span> </a>
                        <ul class="sub">
                            <li><a href="charts-flot.html"><i class="s16 entypo-icon-add"></i><span class="txt">Add Mover</span></a>
                            </li>
                            <li><a href="charts-rickshaw.html"><i class="s16 fa fa-gears"></i><span class="txt">Manage Movers</span></a>
                            </li>

                        </ul>
                    </li>
                    <li>
                        <a href="#"><i class="s16 icomoon-icon-filter"></i><span class="txt">Search Enginewise List</span> </a>
                        <ul class="sub">
                            <li><a href="charts-flot.html"><i class="s16 entypo-icon-add"></i><span class="txt">Add Mover</span></a>
                            </li>
                            <li><a href="charts-rickshaw.html"><i class="s16 fa fa-gears"></i><span class="txt">Manage Movers</span></a>
                            </li>

                        </ul>
                    </li>
                    <li>
                        <a href="#"><i class="s16 wpzoom-settings"></i><span class="txt">Unused/Bad Leads</span> </a>
                        <ul class="sub">
                            <li><a href="charts-flot.html"><i class="s16 entypo-icon-add"></i><span class="txt">Add Mover</span></a>
                            </li>
                            <li><a href="charts-rickshaw.html"><i class="s16 fa fa-gears"></i><span class="txt">Manage Movers</span></a>
                            </li>

                        </ul>
                    </li>
                    <li>
                        <a href="#"><i class="s16 silk-icon-arrow"></i><span class="txt">ReFunding</span> </a>
                        <ul class="sub">
                            <li><a href="charts-flot.html"><i class="s16 entypo-icon-add"></i><span class="txt">Add Mover</span></a>
                            </li>
                            <li><a href="charts-rickshaw.html"><i class="s16 fa fa-gears"></i><span class="txt">Manage Movers</span></a>
                            </li>

                        </ul>
                    </li>
                    <li>
                        <a href="#"><i class="s16 fa fa-magnet"></i><span class="txt">System Automation</span> </a>
                        <ul class="sub">
                            <li><a href="charts-flot.html"><i class="s16 entypo-icon-add"></i><span class="txt">Add Mover</span></a>
                            </li>
                            <li><a href="charts-rickshaw.html"><i class="s16 fa fa-gears"></i><span class="txt">Manage Movers</span></a>
                            </li>

                        </ul>
                    </li>
                    <li>
                        <a href="#"><i class="s16 wpzoom-settings"></i><span class="txt">Settings</span> </a>
                        <ul class="sub">
                            <li><a href="charts-flot.html"><i class="s16 entypo-icon-add"></i><span class="txt">Add Mover</span></a>
                            </li>
                            <li><a href="charts-rickshaw.html"><i class="s16 fa fa-gears"></i><span class="txt">Manage Movers</span></a>
                            </li>

                        </ul>
                    </li>
                    <li>
                        <a href="#"><i class="s16  icomoon-icon-share"></i><span class="txt">Exports/Download</span> </a>
                        <ul class="sub">
                            <li><a href="charts-flot.html"><i class="s16 entypo-icon-add"></i><span class="txt">Add Mover</span></a>
                            </li>
                            <li><a href="charts-rickshaw.html"><i class="s16 fa fa-gears"></i><span class="txt">Manage Movers</span></a>
                            </li>

                        </ul>
                    </li>
                    <li>
                        <a href="#"><i class="s16  icomoon-icon-filter"></i><span class="txt">Lead Status</span> </a>
                        <ul class="sub">
                            <li><a href="charts-flot.html"><i class="s16 entypo-icon-add"></i><span class="txt">Add Mover</span></a>
                            </li>
                            <li><a href="charts-rickshaw.html"><i class="s16 fa fa-gears"></i><span class="txt">Manage Movers</span></a>
                            </li>

                        </ul>
                    </li>
                    <li>
                        <a href="#"><i class="s16  icomoon-icon-filter"></i><span class="txt">Sent To Customer</span> </a>
                        <ul class="sub">
                            <li><a href="charts-flot.html"><i class="s16 entypo-icon-add"></i><span class="txt">Add Mover</span></a>
                            </li>
                            <li><a href="charts-rickshaw.html"><i class="s16 fa fa-gears"></i><span class="txt">Manage Movers</span></a>
                            </li>

                        </ul>
                    </li>
                    <li>
                        <a href="#"><i class="s16 icomoon-icon-cog-2"></i><span class="txt">Deleted Movers</span> </a>
                        <ul class="sub">
                            <li><a href="charts-flot.html"><i class="s16 entypo-icon-add"></i><span class="txt">Add Mover</span></a>
                            </li>
                            <li><a href="charts-rickshaw.html"><i class="s16 fa fa-gears"></i><span class="txt">Manage Movers</span></a>
                            </li>

                        </ul>
                    </li>

                    {{--   <li>
                           <a href="#"><i class="s16 icomoon-icon-list"></i><span class="txt">Forms</span><span class="notification red">6</span></a>
                           <ul class="sub">
                               <li><a href="forms-basic.html"><i class="s16 icomoon-icon-file"></i><span class="txt">Basic forms</span></a>
                               </li>
                               <li><a href="forms-advanced.html"><i class="s16 icomoon-icon-file"></i><span class="txt">Advanced forms</span></a>
                               </li>
                               <li><a href="forms-layouts.html"><i class="s16 icomoon-icon-file"></i><span class="txt">Form layouts</span></a>
                               </li>
                               <li><a href="forms-wizard.html"><i class="s16 fa fa-magic"></i><span class="txt">Form wizard</span></a>
                               </li>
                               <li><a href="forms-validation.html"><i class="s16 fa fa-check"></i><span class="txt">From validation</span></a>
                               </li>
                               <li><a href="code-editor.html"><i class="s16 icomoon-icon-code"></i><span class="txt">Code editor</span></a>
                               </li>
                           </ul>
                       </li>
                       <li>
                           <a href="#"><i class="s16 icomoon-icon-table-2"></i><span class="txt">Tables</span></a>
                           <ul class="sub">
                               <li><a href="tables-basic.html"><i class="s16 icomoon-icon-arrow-right-3"></i><span class="txt">Basic tables</span></a>
                               </li>
                               <li><a href="tables-data.html"><i class="s16 icomoon-icon-arrow-right-3"></i><span class="txt">Data tables</span></a>
                               </li>
                               <li><a href="tables-ajax.html"><i class="s16 icomoon-icon-arrow-right-3"></i><span class="txt">Ajax tables</span></a>
                               </li>
                               <li><a href="tables-pricing.html"><i class="s16 icomoon-icon-arrow-right-3"></i><span class="txt">Pricing tables</span></a>
                               </li>
                           </ul>
                       </li>
                       <li>
                           <a href="#"><i class="s16 icomoon-icon-equalizer-2"></i><span class="txt">UI Elements</span></a>
                           <ul class="sub">
                               <li><a href="icons.html"><i class="s16 icomoon-icon-rocket"></i><span class="txt">Icons</span></a>
                               </li>
                               <li><a href="buttons.html"><i class="s16 icomoon-icon-point-up"></i><span class="txt">Buttons</span></a>
                               </li>
                               <li><a href="tabs.html"><i class="s16 icomoon-icon-tab"></i><span class="txt">Tabs</span></a>
                               </li>
                               <li><a href="accordions.html"><i class="s16 iconic-icon-new-window"></i><span class="txt">Accordions</span></a>
                               </li>
                               <li><a href="modals.html"><i class="s16 cut-icon-popout"></i><span class="txt">Modals</span></a>
                               </li>
                               <li><a href="sliders.html"><i class="s16 fa fa-sliders"></i><span class="txt">Sliders</span></a>
                               </li>
                               <li><a href="progressbars.html"><i class="s16 icomoon-icon-steps"></i><span class="txt">Progressbars</span></a>
                               </li>
                               <li><a href="notifications.html"><i class="s16  icomoon-icon-bubble-notification"></i><span class="txt">Notifications</span></a>
                               </li>
                               <li><a href="typo.html"><i class="s16 icomoon-icon-font"></i><span class="txt">Typography</span></a>
                               </li>
                               <li><a href="lists.html"><i class="s16 icomoon-icon-numbered-list"></i><span class="txt">Lists</span></a>
                               </li>
                               <li><a href="grids.html"><i class="s16 icomoon-icon-grid"></i><span class="txt">Grids</span></a>
                               </li>
                               <li><a href="ui-other.html"><i class="s16 icomoon-icon-arrow-right-3"></i><span class="txt">Other</span></a>
                               </li>
                           </ul>
                       </li>
                       <li><a href="portlets.html"><i class="s16 minia-icon-window-4"></i><span class="txt">Portlets</span></a>
                       </li>
                       <li>
                           <a href="#"><i class="s16 icomoon-icon-envelop"></i><span class="txt">Email</span><span class="notification green">12</span></a>
                           <ul class="sub">
                               <li><a href="email-inbox.html"><i class="s16 fa fa-inbox"></i><span class="txt">Inbox</span></a>
                               </li>
                               <li><a href="email-read.html"><i class="s16 fa fa-mail-forward"></i><span class="txt">Read email</span></a>
                               </li>
                               <li><a href="email-write.html"><i class="s16 fa fa-mail-reply"></i><span class="txt">Write email</span></a>
                               </li>
                           </ul>
                       </li>
                       <li>
                           <a href="#"><i class="s16 icomoon-icon-map"></i><span class="txt">Maps</span></a>
                           <ul class="sub">
                               <li><a href="maps-google.html"><i class="s16 icomoon-icon-arrow-right-3"></i><span class="txt">Google maps</span></a>
                               </li>
                               <li><a href="maps-vector.html"><i class="s16 icomoon-icon-arrow-right-3"></i><span class="txt">Vector maps</span></a>
                               </li>
                           </ul>
                       </li>
                       <li><a href="file.html"><i class="s16 icomoon-icon-upload"></i><span class="txt">File Manager</span></a>
                       </li>
                       <li><a href="widgets.html"><i class="s16 icomoon-icon-cube"></i><span class="txt">Widgets</span><span class="notification red">9</span></a>
                       </li>
                       <li>
                           <a href="#"><i class="s16 icomoon-icon-folder"></i><span class="txt">Pages</span><span class="notification blue">11</span></a>
                           <ul class="sub">
                               <li><a href="blank.html"><i class="s16 icomoon-icon-file-2"></i><span class="txt">Blank page</span></a>
                               </li>
                               <li><a href="calendar.html"><i class="s16 icomoon-icon-calendar"></i><span class="txt">Calendar</span></a>
                               </li>
                               <li><a href="gallery.html"><i class="s16 icomoon-icon-image-2"></i><span class="txt">Gallery</span></a>
                               </li>
                               <li><a href="timeline.html"><i class="s16 entypo-icon-clock"></i><span class="txt">Timeline</span></a>
                               </li>
                               <li><a href="login.html"><i class="s16 icomoon-icon-unlocked"></i><span class="txt">Login</span></a>
                               </li>
                               <li><a href="lock-screen.html"><i class="s16 icomoon-icon-lock"></i><span class="txt">Lock screen</span></a>
                               </li>
                               <li><a href="register.html"><i class="s16 icomoon-icon-user-plus-2"></i><span class="txt">Register</span></a>
                               </li>
                               <li><a href="lost-password.html"><i class="s16 icomoon-icon-file"></i><span class="txt">Lost password</span></a>
                               </li>
                               <li><a href="profile.html"><i class="s16 icomoon-icon-profile"></i><span class="txt">User profile</span></a>
                               </li>
                               <li><a href="invoice.html"><i class="s16 icomoon-icon-file"></i><span class="txt">Invoice</span></a>
                               </li>
                               <li><a href="faq.html"><i class="s16 icomoon-icon-attachment"></i><span class="txt">FAQ</span></a>
                               </li>
                               <li>
                                   <a href="#"><i class="s16 icomoon-icon-file"></i><span class="txt">Error pages</span><span class="notification">6</span></a>
                                   <ul class="sub">
                                       <li><a href="403.html"><i class="s16 icomoon-icon-file"></i><span class="txt">Error 403</span></a>
                                       </li>
                                       <li><a href="404.html"><i class="s16 icomoon-icon-file"></i><span class="txt">Error 404</span></a>
                                       </li>
                                       <li><a href="405.html"><i class="s16 icomoon-icon-file"></i><span class="txt">Error 405</span></a>
                                       </li>
                                       <li><a href="500.html"><i class="s16 icomoon-icon-file"></i><span class="txt">Error 500</span></a>
                                       </li>
                                       <li><a href="503.html"><i class="s16 icomoon-icon-file"></i><span class="txt">Error 503</span></a>
                                       </li>
                                       <li><a href="offline.html"><i class="s16 icomoon-icon-file"></i><span class="txt">Offline page</span></a>
                                       </li>
                                   </ul>
                               </li>
                           </ul>
                       </li>--}}
                </ul>
            </div>
        </div>
        @section('sidebarWidget')
        @show

    </div>
    <!-- End .sidebar-scrollarea -->
@endsection


@section('goBody')
    <br>
    @if(session()->has('messege'))
        <div class="alert alert-success" role="alert">
            <strong>{{session()->get('messege')}}</strong>
        </div>
    @endif


    <div class="addItem text-right">
        <a href="mover/create" class="btn btn-primary"><i style="font-size: xx-large;" class="fas fa-list-ul te"></i></a>
    </div>
    <br>
    <h1 class="text-center bg-secondary text-white">Home Todo List</h1>
    <div class="container">

        <div class="col-md-6 offset-md-3">
            <div class="row">
                <ul class="list-group col-md-8">

                    {{--<li class="bg-{{$bgcol}}">hi</li>--}}
                    @foreach($movers as $i=>$mover)
                        @php
                            $colors = ['danger','warning','success','info','secondary','dark'];
                            //$randomize = $colors[mt_rand(0, count($colors)-1)];
                            $index = array_rand($colors, 1);
                            $selected = $colors[$index];
                            unset($colors[$index]);
                        @endphp
                        <li class="list-group-item bg-{{$selected}}">
                            <a class="text-white" href="{{'/admin/mover/'.$mover->id}}">{{$mover->title}}</a>
                            <span class="pull-right">{{$mover->created_at->diffforHumans()}}</span>

                        </li>

                    @endforeach
                </ul>
                <ul class="list-group col-md-4">
                    @foreach($movers as $mover)

                        <li class="list-group-item">
                            <a href="{{'/admin/mover/'.$mover->id.'/edit'}}"><i class="far fa fa-edit"></i></a>
                            <form class="pull-right" action="{{'/admin/mover/'.$mover->id}}" method="post">
                                {{csrf_field()}}
                                {{method_field('DELETE')}}
                                <button type="submit" style="border: none;padding: 0;background-color: snow;"><i
                                            class="fas fa fa-trash text-success"></i></button>
                            </form>
                        </li>

                    @endforeach
                </ul>
            </div>

        </div>


    </div>
@endsection


{{--@section('goBody')
<h1 class="text-center">i am before main content</h1>
@endsection

@section('goHeader')
    <h3 class="text-center">i am on Head</h3>
@endsection--}}
