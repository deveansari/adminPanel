<!DOCTYPE html>
<html lang="en">
<head>
    @section('title', 'Admin Login')
    @include('admin.layouts.head')
</head>
<body class="login-page">

<div id="header" class="animated fadeInDown">
    <div class="row">
        <div class="navbar">
            <div class="container text-center">
                <a class="navbar-brand" href="dashboard.html">Supr.<span class="slogan">admin</span></a>
            </div>
        </div>
        <!-- /navbar -->
    </div>
    <!-- End .row -->
</div>
<!-- End #header -->
<!-- Start login container -->
<div class="container login-container">
    <div class="login-panel panel panel-default plain animated bounceIn">
        <!-- Start .panel -->
        <div class="panel-body">
            @include('admin.includes.messages')
            <form method="POST" class="form-horizontal mt0" action="{{ route('admin.login') }}" aria-label="{{ __('Login') }}" id="login-form">
                @csrf
                <div class="form-group">
                    <div class="col-md-12">
                        <!-- col-md-12 start here -->
                        <label for="">{{ __('E-Mail Address') }}:</label>
                    </div>
                    <!-- col-md-12 end here -->
                    <div class="col-lg-12">
                        <div class="input-group input-icon">
                            <input type="email" name="email" id="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" value="{{ old('email') }}"
                                   placeholder="Enter email ...">
                            @if ($errors->has('email'))
                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                            @endif
                            <span class="input-group-addon"><i class="icomoon-icon-user s16"></i></span>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-md-12">
                        <!-- col-md-12 start here -->
                        <label for="password">{{ __('Password') }}</label>
                    </div>
                    <!-- col-md-12 end here -->
                    <div class="col-lg-12">
                        <div class="input-group input-icon">
                            <input type="password" name="password" id="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" value="somepass"
                                   placeholder="Your password" required>

                            @if ($errors->has('password'))
                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                            @endif
                            <span class="input-group-addon"><i class="icomoon-icon-lock s16"></i></span>
                        </div>
                        <span class="help-block text-right"><a href="lost-password.html">Forgout password ?</a></span>
                    </div>
                </div>
                <div class="form-group mb0">
                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-8">
                        <div class="checkbox-custom">
                            <input type="checkbox" name="remember" id="remember" value="option" {{ old('remember') ? 'checked' : '' }}>
                            <label for="remember">{{ __('Remember Me') }} ?</label>
                        </div>
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-4 mb25">
                        <button class="btn btn-default pull-right" type="submit">{{ __('Login') }}</button>
                    </div>
                </div>
            </form>
            <div class="seperator">
                <strong>or</strong>
                <hr>
            </div>
            <div class="social-buttons text-center mt5 mb5">
                <a href="#" class="btn btn-primary btn-alt mr10">Sign in with <i class="fa fa-facebook s20 ml5 mr0"></i></a>
                <a href="#" class="btn btn-danger btn-alt ml10">Sign in with <i
                            class="fa fa-google-plus s20 ml5 mr0"></i></a>
            </div>
        </div>
        <div class="panel-footer gray-lighter-bg">
            <h4 class="text-center"><strong>Don`t have an account ?</strong>
            </h4>
            <p class="text-center"><a href="register.html" class="btn btn-success">Create account</a>
            </p>
        </div>
    </div>
    <!-- End .panel -->
</div>
<!-- End login container -->

{{--<form method="POST" action="{{ route('admin.login') }}" aria-label="{{ __('Login') }}">
    @csrf

    <div class="form-group row">
        <label for="email" class="col-sm-4 col-form-label text-md-right">{{ __('E-Mail Address') }}</label>

        <div class="col-md-6">
            <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required>

            @if ($errors->has('email'))
                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
            @endif
        </div>
    </div>

    <div class="form-group row">
        <label for="password" class="col-md-4 col-form-label text-md-right">{{ __('Password') }}</label>

        <div class="col-md-6">
            <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required>

            @if ($errors->has('password'))
                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
            @endif
        </div>
    </div>

    <div class="form-group row">
        <div class="col-md-6 offset-md-4">
            <div class="checkbox">
                <label>
                    <input type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }}> {{ __('Remember Me') }}
                </label>
            </div>
        </div>
    </div>

    <div class="form-group row mb-0">
        <div class="col-md-8 offset-md-4">
            <button type="submit" class="btn btn-primary">
                {{ __('Login') }}
            </button>

            <a class="btn btn-link" href="{{ route('password.request') }}">
                {{ __('Forgot Your Password?') }}
            </a>
        </div>
    </div>
</form>--}}



@include('admin.layouts.footer')
@section('loginfooter')
    <!-- Javascripts -->

    <!--[if lt IE 9]>
    <script type="text/javascript" src="js/libs/excanvas.min.js"></script>
    <script type="text/javascript" src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <script type="text/javascript" src="js/libs/respond.min.js"></script>
    <![endif]-->
    <!-- Bootstrap plugins -->
    <script src="{{('admin/html/js/bootstrap/bootstrap.js')}}"></script>
    <!-- Form plugins -->
    <script src="{{asset('admin/html/plugins/forms/validation/jquery.validate.js')}}"></script>
    <script src="{{asset('admin/html/plugins/forms/validation/additional-methods.min.js')}}"></script>
    <!-- Init plugins olny for this page -->
    <script src="{{asset('admin/html/js/pages/login.js')}}"></script>
@endsection

</body>
</html>