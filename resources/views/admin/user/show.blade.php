<!doctype html>
<html lang="en">
<head>
    @include('admin.layouts.head')
</head>
<body>
@include('admin.layouts.header')
<div id="wrapper">

@include('admin.layouts.sidebar')
<!-- End #right-sidebar -->

    <!--Body content-->

    <div id="content" class="page-content clearfix">
        <div class="contentwrapper">
            <!--Content wrapper-->
            <div class="heading">
                <!--  .heading-->
                <h3>Data tables</h3>
                <div class="resBtnSearch">
                    <a href="#"><span class="s16 icomoon-icon-search-3"></span></a>
                </div>
                <div class="search">
                    <!-- .search -->
                    <form id="searchform" class="form-horizontal" action="search.html">
                        <input type="text" class="top-search from-control" placeholder="Search here ..."/>
                        <input type="submit" class="search-btn" value=""/>
                    </form>
                </div>
                <!--  /search -->
                <ul class="breadcrumb">
                    <li>You are here:</li>
                    <li>
                        <a href="#" class="tip" title="back to dashboard">
                            <i class="s16 icomoon-icon-screen-2"></i>
                        </a>
                        <span class="divider">
                <i class="s16 icomoon-icon-arrow-right-3"></i>
            </span>
                    </li>
                    <li class="active">Blank Page</li>
                </ul>
            </div>
            <!-- End  / heading-->
            <!-- Start .row -->
            <div class="row">
                <div class="col-lg-12">
                    <!-- col-lg-12 start here -->
                    <div class="panel panel-default toggle panelMove panelClose panelRefresh">
                        <!-- Start .panel -->
                        <div class="panel-heading">
                            <h4 class="panel-title">Basic Data tables</h4>
                        </div>
                        <div class="panel-body">
                            <table id="basic-datatables" class="table table-striped table-bordered" cellspacing="0"
                                   width="100%">
                                <thead>
                                <tr>
                                    <th>Id</th>
                                    <th>Name</th>
                                    <th>Email</th>
                                </tr>
                                </thead>

                                <tbody>
                                @foreach ($admins as $admin)
                                    <tr>
                                        <td>#{{ $loop->index + 1 }}</td>
                                        <td>{{ $admin->name }}</td>
                                        <td>{{ $admin->email }}</td>

                                    </tr>

                                @endforeach

                                </tbody>

                            </table>
                            <a name="" id="" class="btn btn-info" href="#"
                               role="button"><i aria-hidden="true" class="icomoon-icon-user-plus-2"></i>Add User</a>
                        </div>
                    </div>
                    <!-- End .panel -->
                </div>
                <!-- col-lg-12 end here -->


            </div>
            {{--<div class="row">
                <div class="col-md-12">
                    <div class="box">
                        <div class="box-header with-border">
                            <h3 class="box-title">Users</h3>
                            <a class='col-lg-offset-5 btn btn-success' href="{{ route('user.create') }}">Add New</a>
                            @include('includes.messages')
                            <div class="box-tools pull-right">
                                <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
                                    <i class="fa fa-minus"></i></button>
                                <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
                                    <i class="fa fa-times"></i></button>
                            </div>
                        </div>
                        <div class="box-body">
                            <div class="box">
                                <div class="box-header">
                                    <h3 class="box-title">Data Table With Full Features</h3>
                                </div>
                                <!-- /.box-header -->
                                <div class="box-body">
                                    <table id="example1" class="table table-bordered table-striped">
                                        <thead>
                                        <tr>
                                            <th>S.No</th>
                                            <th>User Name</th>
                                            <th>Assigned Roles</th>
                                            <th>Status</th>
                                            <th>Edit</th>
                                            <th>Delete</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @foreach ($users as $user)
                                            <tr>
                                                <td>{{ $loop->index + 1 }}</td>
                                                <td>{{ $user->name }}</td>
                                                <td>
                                                    @foreach ($user->roles as $role)
                                                        {{ $role->name }},
                                                    @endforeach
                                                </td>
                                                <td>{{ $user->status? 'Active' : 'Not Active' }}</td>
                                                <td><a href="{{ route('user.edit',$user->id) }}"><span class="glyphicon glyphicon-edit"></span></a></td>
                                                <td>
                                                    <form id="delete-form-{{ $user->id }}" method="post" action="{{ route('user.destroy',$user->id) }}" style="display: none">
                                                        {{ csrf_field() }}
                                                        {{ method_field('DELETE') }}
                                                    </form>
                                                    <a href="" onclick="
                                                            if(confirm('Are you sure, You Want to delete this?'))
                                                            {
                                                            event.preventDefault();
                                                            document.getElementById('delete-form-{{ $user->id }}').submit();
                                                            }
                                                            else{
                                                            event.preventDefault();
                                                            }" ><span class="glyphicon glyphicon-trash"></span></a>
                                                </td>
                                            </tr>
                                            </tr>
                                        @endforeach
                                        </tbody>
                                        <tfoot>
                                        <tr>
                                            <th>S.No</th>
                                            <th>User Name</th>
                                            <th>Assigned Roles</th>
                                            <th>Status</th>
                                            <th>Edit</th>
                                            <th>Delete</th>
                                        </tr>
                                        </tfoot>
                                    </table>
                                </div>
                                <!-- /.box-body -->
                            </div>
                        </div>
                        <!-- /.box-body -->
                        <div class="box-footer">
                            Footer
                        </div>
                        <!-- /.box-footer-->
                    </div>
                </div>
            </div>--}}

        <!--Content wrapper-->
            <div class="heading">
                <!--  .heading-->
                <h3>Data tables</h3>
                <div class="resBtnSearch">
                    <a href="#"><span class="s16 icomoon-icon-search-3"></span></a>
                </div>
                <div class="search">
                    <!-- .search -->
                    <form id="searchform" class="form-horizontal" action="search.html">
                        <input type="text" class="top-search from-control" placeholder="Search here ..."/>
                        <input type="submit" class="search-btn" value=""/>
                    </form>
                </div>
                <!--  /search -->
                <ul class="breadcrumb">
                    <li>You are here:</li>
                    <li>
                        <a href="#" class="tip" title="back to dashboard">
                            <i class="s16 icomoon-icon-screen-2"></i>
                        </a>
                        <span class="divider">
                <i class="s16 icomoon-icon-arrow-right-3"></i>
            </span>
                    </li>
                    <li class="active">Blank Page</li>
                </ul>
            </div>
            <!-- End  / heading-->
            <!-- Start .row -->
            <div class="row">
                <div class="col-lg-12">
                    <!-- col-lg-12 start here -->
                    <div class="panel panel-default toggle panelMove panelClose panelRefresh">
                        <!-- Start .panel -->
                        <div class="panel-heading">
                            <h4 class="panel-title">Basic Data tables</h4>
                        </div>
                        <div class="panel-body">
                            <table id="basic-datatables" class="table table-striped table-bordered" cellspacing="0"
                                   width="100%">
                                <thead>
                                <tr>
                                    <th>Id</th>
                                    <th>Name</th>
                                    <th>Email</th>
                                </tr>
                                </thead>
                                @foreach ($users as $user)
                                    <tr>
                                        <td>#{{ $loop->index + 1 }}</td>
                                        <td>{{ $user->name }}</td>
                                        <td>{{ $user->email }}</td>

                                    </tr>

                                @endforeach
                                <tbody>

                                </tbody>

                            </table>
                            <a name="" id="" class="btn btn-info" href="#"
                               role="button"><i aria-hidden="true" class="icomoon-icon-user-plus-2"></i>Add User</a>
                        </div>
                    </div>
                    <!-- End .panel -->
                </div>
                <!-- col-lg-12 end here -->


            </div>
            <!-- End .row -->
        </div>
        <!-- End contentwrapper -->
    </div>

    <!--Body content-->
    @include('admin.layouts.footer')
</div>
</body>
</html>