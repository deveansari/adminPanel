@extends('admin.layouts.app')
@section('title', 'Inside mover')
@section('goBody')
    <br>
    <h5 class="text-center text-info">Inside mover table through id</h5>
    <hr>
    <div class="container">

        <div class="row">

            <div class="col-md-4 offset-md-4">
                <div class="list-group">
                    <a href="#" class="list-group-item">
                        <div class="d-flex w-100">
                            <h5 class="mb-1">{{$item->title}}</h5>
                            {{--<small></small>--}}
                        </div>
                        <p class="mb-1">{{$item->body}}</p>
                        <small>{{$item->created_at->diffforHumans()}}</small>
                    </a>

                </div>
            </div>

        </div>


    </div>
@endsection