
<div class="container">
    <div class="footer">
        <p class="text-center">&copy;2015 Copyright Supr.admin. All right reserved !!!</p>
    </div>
</div>
<!-- Javascripts -->
<!-- Load pace first -->
<script src="{{asset('admin/html/plugins/core/pace/pace.min.js')}}"></script>
<!-- Important javascript libs(put in all pages) -->
<script src="{{asset('admin/html/js/libs/jquery-2.1.1.min.js')}}"></script>
<script>
    window.jQuery || document.write('<script src="{{asset('admin/html/js/libs/jquery-2.1.1.min.js')}}">\x3C/script>')
</script>
<script src="{{asset('admin/html/js/libs/jquery-ui-1.10.4.min.js')}}"></script>
<script>
    window.jQuery || document.write('<script src="{{asset('admin/html/js/libs/jquery-ui-1.10.4.min.js')}}">\x3C/script>')
</script>
<script src="{{asset('admin/html/js/libs/jquery-migrate-1.2.1.min.js')}}"></script>
<script>
    window.jQuery || document.write('<script src="{{asset('admin/html/js/libs/jquery-migrate-1.2.1.min.js')}}">\x3C/script>')
</script>
<!--[if lt IE 9]>
<script type="text/javascript" src="{{asset('admin/html/js/libs/excanvas.min.js')}}"></script>
<script type="text/javascript" src="{{asset('admin/html/html5shim.googlecode.com/svn/trunk/html5.js')}}"></script>
<script type="text/javascript" src="{{asset('admin/html/js/libs/respond.min.js')}}"></script>
<![endif]-->
<!-- Bootstrap plugins -->
<script src="{{asset('admin/html/js/bootstrap/bootstrap.js')}}"></script>
<!-- Core plugins ( not remove ) -->
<script src="{{asset('admin/html/js/libs/modernizr.custom.js')}}"></script>
<!-- Handle responsive view functions -->
<script src="{{asset('admin/html/js/jRespond.min.js')}}"></script>
<!-- Custom scroll for sidebars,tables and etc. -->
<script src="{{asset('admin/html/plugins/core/slimscroll/jquery.slimscroll.min.js')}}"></script>
<script src="{{asset('admin/html/plugins/core/slimscroll/jquery.slimscroll.horizontal.min.js')}}"></script>
<!-- Remove click delay in touch -->
<script src="{{asset('admin/html/plugins/core/fastclick/fastclick.js')}}"></script>
<!-- Increase jquery animation speed -->
<script src="{{asset('admin/html/plugins/core/velocity/jquery.velocity.min.js')}}"></script>
<!-- Quick search plugin (fast search for many widgets) -->
<script src="{{asset('admin/html/plugins/core/quicksearch/jquery.quicksearch.js')}}"></script>
<!-- Bootbox fast bootstrap modals -->
<script src="{{asset('admin/html/plugins/ui/bootbox/bootbox.js')}}"></script>
<!-- Other plugins ( load only nessesary plugins for every page) -->
<script src="{{asset('admin/html/plugins/charts/sparklines/jquery.sparkline.js')}}"></script>
<script src="{{asset('admin/html/plugins/charts/knob/jquery.knob.js')}}"></script>
<script src="{{asset('admin/html/plugins/charts/flot/jquery.flot.custom.js')}}"></script>
<script src="{{asset('admin/html/plugins/charts/flot/jquery.flot.pie.js')}}"></script>
<script src="{{asset('admin/html/plugins/charts/flot/jquery.flot.resize.js')}}"></script>
<script src="{{asset('admin/html/plugins/charts/flot/jquery.flot.time.js')}}"></script>
<script src="{{asset('admin/html/plugins/charts/flot/jquery.flot.growraf.js')}}"></script>
<script src="{{asset('admin/html/plugins/charts/flot/jquery.flot.categories.js')}}"></script>
<script src="{{asset('admin/html/plugins/charts/flot/jquery.flot.stack.js')}}"></script>
<script src="{{asset('admin/html/plugins/charts/flot/jquery.flot.orderBars.js')}}"></script>
<script src="{{asset('admin/html/plugins/charts/flot/jquery.flot.tooltip.min.js')}}"></script>
<script src="{{asset('admin/html/plugins/ui/waypoint/waypoints.js')}}"></script>
<script src="{{asset('admin/html/plugins/forms/autosize/jquery.autosize.js')}}"></script>
<script src="{{asset('admin/html/js/jquery.supr.js')}}"></script>
<script src="{{asset('admin/html/js/main.js')}}"></script>
<script src="{{asset('admin/html/js/pages/dashboard.js')}}"></script>


<!-- Javascripts -->

@section('loginfooter')
    @show
